<?php

namespace classes\JSONRPC\Endpoints;
use ShadowCMS\JSONRPC\Server;
use ShadowCMS\JSONRPC\Plugins\AuthenticationPlugin;
use ShadowCMS\JSONRPC\Plugins\FilterPlugin;
use Controllers;

class PrivateEndpoint extends Server
{
	function allowedFunctions()
	{
		return array(
			"file" => \Controllers\FileController::class,
			"image" => \Controllers\ImageController::class,
			"user" => \Controllers\UserController::class,
			"activity" => \Controllers\ActivityController::class,
			"location" => \Controllers\LocationController::class,
		);
	}

	function addPlugins()
	{
		$this->addPlugin(new AuthenticationPlugin(
			function ($strToken){
				$session = new \Session($GLOBALS["session_cookie_name"]);
				$session->load($strToken);
				if (!session_id()) session_start();
				$GLOBALS["session"] = $session;
				return $session->isValid();
			},
			$GLOBALS["session_cookie_name"]
		));
		$this->addPlugin(new FilterPlugin(array(
			"user_password",
			"user_salt"
		)));
	}
}

$jsonRPC = new PrivateEndpoint("logs/");
$jsonRPC->start();