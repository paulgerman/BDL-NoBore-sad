<?php

namespace classes\JSONRPC\Endpoints;
use \ShadowCMS\JSONRPC\Server;
use Controllers;
class PublicEndpoint extends Server
{
	function allowedFunctions()
	{
		return array(
			"accessLog_add" => array(\Controllers\AccessLogController::class, "add"),
		);
	}

	function addPlugins()
	{
	}
}
$jsonRPC = new PublicEndpoint("logs/");
$jsonRPC->start();
