class Activity extends Renderer{
	init()
	{
		call("activity_get_all", [0,99999], (arrItems) => {
			this.arrItems = arrItems;
			this.startRender();
		});
	}

	startRender()
	{
		this.elParent.appendChild(createElement("h1", {}, "Add/Edit"));

		let arrActions = [
			{
				"text" : "Edit",
				"callback" : (nKey, elButton) => {
					$(this.form.elForm).goTo();
					this.form.loadState({arrData: this.arrItems[nKey], bEditMode: true});
					elButton.disabled = false;
				}
			},
			{
				"text" : "Delete",
				"callback" : this.deleteItem.bind(this),
				"class": "btn-delete"
			},
            (nKey, elParent) => {
                elParent.appendChild(createElement(
                    "button", {class: "btn btn-primary btn-sm", style: "margin-bottom:2px;"}, "Edit locations",
                    {click: (e) => {
                            if(this.table)
                                this.table.dataTable.destroy();
                            $("#container").empty();
                            let renderer = new Location(document.getElementById("container"), {activity_id: this.arrItems[nKey]["activity_id"], activity_name: this.arrItems[nKey]["activity_name"]}, this.arrRenderConfigs);
                            renderer.init();
                        }}
                ));
                elParent.appendChild(createElement("br"));
            }
		];

		this.renderItems(this.elParent, arrActions);

		this.form.fnEditCallback = (nID, arrObject) => {
			call("activity_edit", [nID, arrObject], (arrResult) => {
				let strIndexKey = this.arrRenderConfig["indexKey"];
				let nIndex = arrResult[strIndexKey];
				this.table.removeItem(nIndex, false);
				this.table.addItem(arrResult);
				this.table.dataTable.draw(false);

				this.form.resetState();

				this.arrItems[nIndex] = arrResult;
			});
		};

		this.form.fnAddCallback = (arrObject) => {
			call("activity_create", [arrObject], (arrResult) => {
				let strIndexKey = this.arrRenderConfig["indexKey"];
				let nIndex = arrResult[strIndexKey];
				this.table.addItem(arrResult);
				this.form.resetState();

				this.arrItems[nIndex] = arrResult;
				this.table.dataTable.draw(false);
			});
		};
	}

	renderItems(elParent, arrActions)
	{
		this.form = new Form(
			elParent,
			this.arrRenderConfig,
			{},
			[]
		);

		this.table = new Table(
			elParent,
			this.arrItems,
			this.arrRenderConfig,
			arrActions,
			[],
			{}
		);
		this.form.render();
		elParent.appendChild(createElement("hr"));
		elParent.appendChild(createElement("h1", {}, "Activities"));
		this.table.render();
	}

	deleteItem(nItemID, elButton)
	{
		if (confirm("Are you sure?") === true)
		{
			call(
				"activity_delete",
				[nItemID],
				() => {
					this.table.removeItem(nItemID, true);
					delete this.arrItems[nItemID];

					if(this.reorder !== undefined)
						this.reorder.removeItem(nItemID);
				},
				() => {
					elButton.disabled = false;
				}
			);
		}
		else
		{
			elButton.disabled = false;
		}
	}
}