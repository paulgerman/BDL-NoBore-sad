<?php

namespace Controllers;
use Objects\Image;
use ShadowCMS\BaseController;

class ImageController extends BaseController
{
	function __construct()
	{
		$this->strClassName = null;
	}

	function create($arrData)
	{
		$this->processFile($arrData, "image_data", "image");
		return parent::create($arrData);
	}

	function edit($strID, $arrData)
	{
		$this->processFile($arrData, "image_data", "image");
		return parent::edit($strID, $arrData);
	}

	function upload($nMaxWidthOrHeight = 3086)
	{
		$arrData = array(
			"image_data" => $_FILES["image"],
			"maxWidthOrHeight" => $nMaxWidthOrHeight
		);
		$this->processFile($arrData, "image_data", "image");
		return $arrData["image_data"];
	}

	function uploadMultiple()
	{
		$arrFiles = $this->normalizeFiles($_FILES["image"]);

		$arrData = array(
			"image_data" => $arrFiles
		);
		foreach($arrData["image_data"] as $strKey => $arrFile)
		{
			$this->processFile($arrData["image_data"], $strKey, "image");
		}
		return $arrData["image_data"];
	}

	private function normalizeFiles( &$files )
	{
		$_files = [ ];
		$_files_count = count( $files[ 'name' ] );
		$_files_keys = array_keys( $files );

		for ( $i = 0; $i < $_files_count; $i++ )
			foreach ( $_files_keys as $key )
				$_files[ $i ][ $key ] = $files[ $key ][ $i ];

		return $_files;
	}
}