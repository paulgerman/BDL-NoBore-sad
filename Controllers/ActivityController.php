<?php

namespace Controllers;
use Objects\Activity;
use Objects\Event;
use Objects\UserSession;
use ShadowCMS\BaseController;

class ActivityController extends BaseController
{
	function __construct()
	{
		$this->strClassName = Activity::class;
	}
}