<?php
namespace Controllers
{

	use ShadowCMS\BaseController;

	class FileController extends BaseController
	{
		function __construct()
		{
			$this->strClassName = "\\Objects\\File";
		}
		function create($arrData)
		{
			$this->processFile($arrData, "file_path", "file");
			return parent::create($arrData);
		}

		function edit($strID, $arrData)
		{
			$this->processFile($arrData, "file_path", "file");
			return parent::edit($strID, $arrData);
		}

		function upload()
		{
			$arrData = array(
				"file_path" => $_FILES["file"]
			);
			$this->processFile($arrData, "file_path", "file");
			return $arrData["file_path"];
		}
	}
}