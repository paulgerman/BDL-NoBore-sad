<?php

namespace Controllers;
use Objects\Notification;
use Objects\UserSession;
use ShadowCMS\BaseController;

class NotificationController extends BaseController
{
	function __construct()
	{
		$this->strClassName = Notification::class;
	}

	function send($strUserID, $strSubject, $strMessage, $strLink)
	{
		$controllerUser = new UserController();
		$arrNotification = $this->create([
			"notification_name" => $strSubject,
			"notification_message" => $strMessage,
			"notification_link" => $strLink,
			"user_id" => $strUserID
		]);

		$arrUserTo = $controllerUser->get($strUserID);

		$controllerUser->sendPushNotification($arrUserTo, $arrNotification);

		$controllerUser = new UserController();
		$controllerUser->edit($strUserID, ["user_has_notifications" => "yes"]);

		return $arrNotification;
	}
}