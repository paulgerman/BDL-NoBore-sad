<?php

namespace Controllers;
use Objects\User;
use Objects\UserSession;
use Objects\Wish;
use ShadowCMS\BaseController;
use ShadowCMS\GeneralException;

class WishController extends BaseController
{
	function __construct()
	{
		$this->strClassName = Wish::class;
	}

	function getLocation($arrWish, $arrWishMatch)
	{
		$nLocationID = NULL;
		$controllerLocation = new LocationController();

		if(!empty($arrWish["activity_location_id"]))
			$nLocationID = $arrWish["activity_location_id"];
		elseif(!empty($arrWishMatch["activity_location_id"]))
			$nLocationID = $arrWishMatch["activity_location_id"];

		if($nLocationID === NULL)
			$arrLocation = $controllerLocation->get(["activity_id" => $arrWish["activity_id"], "city_id" => $arrWish["city_id"]]);
		else
			$arrLocation = $controllerLocation->get($nLocationID);

		if($arrLocation["activity_location_images"] !== '')
			$arrLocation["activity_location_images"] = explode(",", $arrLocation["activity_location_images"]);
		else
			$arrLocation["activity_location_images"] = [];

		return $arrLocation;
	}

	function doRating($arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;

		$controllerLocationReview = new \Controllers\LocationReviewController();
		$controllerUserReview = new \Controllers\UserReviewController();

		foreach($_POST["review_location"] as $nWishID => $nLocationRating)
		{
			$arrWish = $this->get($nWishID);
			$arrWishMatch = $this->get($arrWish["match_wish_id"]);
			$nActivityLocationID = $this->getLocation($arrWish, $arrWishMatch)["activity_location_id"];

			$controllerLocationReview->create([
				"user_id" => $arrUser["user_id"],
				"activity_location_review_rating" => $nLocationRating,
				"activity_location_id" => $nActivityLocationID,
			]);

			$controllerUserReview->create([
				"user_id" => $arrUser["user_id"],
				"user_review_rating" => $_POST["review_user"][$nWishID],
				"user_id_reviewed" => $arrWishMatch["user_id"],
			]);

			$arrWish["wish_reviewed"] = "yes";
			$this->edit($nWishID, $arrWish);
		}

		header("Location: ".buildURL()."&ok=1");
	}

	function getMyRequestedWishes($nActivityID = NULL, $arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;
		return $this->get_all_internal(0, 9999, " WHERE `wish_status` = 'requested' AND `user_id` = " . (int)$arrUser["user_id"] . (is_null($nActivityID)?"":" AND `activity_id`=" . (int)$nActivityID));
	}

	function getMyPendingWishes($nActivityID = NULL, $arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;
		return $this->get_all_internal(0, 9999, " WHERE `wish_status` = 'pending' AND `user_id` = " . (int)$arrUser["user_id"] . (is_null($nActivityID)?"":" AND `activity_id`=" . (int)$nActivityID));
	}
	function getMyAcceptedWishes($nActivityID = NULL, $arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;
		return $this->get_all_internal(0, 9999, " WHERE `wish_status` = 'accepted' AND `user_id` = " . (int)$arrUser["user_id"] . (is_null($nActivityID)?"":" AND `activity_id`=" . (int)$nActivityID));
	}


	function runMatchingAlgorithm($arrRequestedWishes, $arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;
		$pdo = trdb();
		$controllerNotification = new NotificationController();
		foreach ($arrRequestedWishes as &$arrMyRequestedWish)
		{
			$strQueryPart = "";
			if($arrMyRequestedWish["activity_location_id"] != NULL)
				$strQueryPart .= " AND (`activity_location_id` IS NULL OR `activity_location_id` = ".$arrMyRequestedWish["activity_location_id"].")";

			$arrMatchingWishes = $this->get_all_internal(0, 99999, " WHERE `wish_status` = 'requested' AND `activity_id`=" . (int)$arrMyRequestedWish["activity_id"] . " AND `user_id` != " . (int)$arrUser["user_id"] . " AND `wish_end_timestamp` > " . $pdo->quote($arrMyRequestedWish["wish_start_timestamp"]) . " AND `wish_start_timestamp` < " . $pdo->quote($arrMyRequestedWish["wish_end_timestamp"]).$strQueryPart);

			foreach ($arrMatchingWishes as $arrMatchingWish)
			{
				$arrMatchingWish["wish_status"] = "pending";
				$arrMatchingWish["match_wish_id"] = $arrMyRequestedWish["wish_id"];
				$arrMyRequestedWish["wish_status"] = "pending";
				$arrMyRequestedWish["match_wish_id"] = $arrMatchingWish["wish_id"];
				$this->edit($arrMatchingWish["wish_id"], $arrMatchingWish);
				$this->edit($arrMyRequestedWish["wish_id"], $arrMyRequestedWish);

				$controllerNotification->send($arrMatchingWish["user_id"], "Match found", "New match found!", "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
				header("Refresh:0");
				die();
				break;
			}
		}
	}

	function acceptWish($arrWish, $arrUser = NULL)
	{
		if(is_null($arrUser)) $arrUser = $GLOBALS["session"]->arrUser;

		$controllerNotification = new NotificationController();
		$controllerUser = new UserController();
		$controllerActivity = new ActivityController();
		if ($arrWish["user_id"] === $arrUser["user_id"] && $arrWish["wish_status"] == "pending")
		{
			$arrMatchWish = $this->get($arrWish["match_wish_id"]);
			$arrMatchUser = $controllerUser->get($arrMatchWish["user_id"]);
			$arrActivity = $controllerActivity->get($arrWish["activity_id"]);

			$controllerNotification->send($arrMatchWish["user_id"], "Match accepted", "Your match for " . $arrActivity["activity_name"] . " has accepted to hang out with you", "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

			$arrWish["wish_status"] = "accepted";
			$this->edit($arrWish["wish_id"], $arrWish);

			if($arrMatchWish["wish_status"] === "accepted")
			{
				$controllerBusinessLocation = new BusinessLocationController();
				$arrLocation = $this->getLocation($arrWish, $arrMatchWish);
				$arrBusinessLocation = NULL;
				try
				{
					$arrBusinessLocation = $controllerBusinessLocation->get(["activity_location_id" => $arrLocation["activity_location_id"]]);
				}
				catch (GeneralException $exc)
				{
					if($exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND)
						throw $exc;
				}
				if(!is_null($arrBusinessLocation))
					$controllerNotification->send($arrBusinessLocation["user_id"], "New customers coming!", "You have 2 new customers for " . $arrActivity["activity_name"], "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
			}
		}
	}

	function displayRatingForm($arrWish, $arrMatchUser)
	{
		$controllerUser = new UserController();
		echo '
<form action="'.buildURL().'" method="post">
    <input type="hidden" name="review" value="yes">
    <div style="font-size: 17px; padding: 4px;">
        Activity review:
        <br>
        <div class="stars">
            <input class="star star-5" id="star-5-'.$arrWish["wish_id"].'" type="radio" name="review_location['.$arrWish["wish_id"].']" value="5" required/>
            <label class="star star-5" for="star-5-'.$arrWish["wish_id"].'"></label>
            <input class="star star-4" id="star-4-'.$arrWish["wish_id"].'" type="radio" name="review_location['.$arrWish["wish_id"].']" value="4"/>
            <label class="star star-4" for="star-4-'.$arrWish["wish_id"].'"></label>
            <input class="star star-3" id="star-3-'.$arrWish["wish_id"].'" type="radio" name="review_location['.$arrWish["wish_id"].']" value="3"/>
            <label class="star star-3" for="star-3-'.$arrWish["wish_id"].'"></label>
            <input class="star star-2" id="star-2-'.$arrWish["wish_id"].'" type="radio" name="review_location['.$arrWish["wish_id"].']" value="2"/>
            <label class="star star-2" for="star-2-'.$arrWish["wish_id"].'"></label>
            <input class="star star-1" id="star-1-'.$arrWish["wish_id"].'" type="radio" name="review_location['.$arrWish["wish_id"].']" value="1"/>
            <label class="star star-1" for="star-1-'.$arrWish["wish_id"].'"></label>
        </div>
        <br>
        Review your peer: ' . $controllerUser->displayUserInline($arrMatchUser) . '
        <br>
        <div class="stars">
            <input class="star star-5" id="star2-5'.$arrWish["wish_id"].'" type="radio" name="review_user['.$arrWish["wish_id"].']" value="5" required/>
            <label class="star star-5" for="star2-5'.$arrWish["wish_id"].'"></label>
            <input class="star star-4" id="star2-4'.$arrWish["wish_id"].'" type="radio" name="review_user['.$arrWish["wish_id"].']" value="4"/>
            <label class="star star-4" for="star2-4'.$arrWish["wish_id"].'"></label>
            <input class="star star-3" id="star2-3'.$arrWish["wish_id"].'" type="radio" name="review_user['.$arrWish["wish_id"].']" value="3"/>
            <label class="star star-3" for="star2-3'.$arrWish["wish_id"].'"></label>
            <input class="star star-2" id="star2-2'.$arrWish["wish_id"].'" type="radio" name="review_user['.$arrWish["wish_id"].']" value="2"/>
            <label class="star star-2" for="star2-2'.$arrWish["wish_id"].'"></label>
            <input class="star star-1" id="star2-1'.$arrWish["wish_id"].'" type="radio" name="review_user['.$arrWish["wish_id"].']" value="1"/>
            <label class="star star-1" for="star2-1'.$arrWish["wish_id"].'"></label>
        </div>
        <br>
        <button style="margin-top: 10px;" type="submit" class="btn btn-success">Submit</button>
    </div>

</form>';
	}
	
	function displayPendingWishes($arrPendingWishes)
	{
		$controllerUser = new UserController();

		if (count($arrPendingWishes))
		{
			foreach ($arrPendingWishes as $arrMyWish)
			{
				echo '
	<div class="panel">';
				$arrMatchWish = $this->get($arrMyWish["match_wish_id"]);
				$arrMatchUser = $controllerUser->get($arrMatchWish["user_id"]);

				$strStartTime = ($arrMyWish["wish_start_timestamp"] > $arrMatchWish["wish_start_timestamp"]) ? $arrMyWish["wish_start_timestamp"] : $arrMatchWish["wish_start_timestamp"];
				$strEndTime = ($arrMyWish["wish_end_timestamp"] < $arrMatchWish["wish_end_timestamp"]) ? $arrMyWish["wish_end_timestamp"] : $arrMatchWish["wish_end_timestamp"];

				echo '
		<div class="boxYesNo" style="padding: 4px;">
			<div class="left" style="font-size: 17px;">
				We have found a match for you. Would you like to do this activity with user ' . $controllerUser->displayUserInline($arrMatchUser) . '?
				<br>Requested time: ' . $this->displayDateTimeInterval($arrMyWish["wish_start_timestamp"], $arrMyWish["wish_end_timestamp"]) . '
				<br>Available time: ' . $this->displayDateTimeInterval($strStartTime, $strEndTime);
				if ($arrMatchWish["wish_status"] === "accepted")
					echo '
				<br>Your match has already accepted to hang out with you :)';

				echo '
			</div>
			<div class="right" style="text-align: center;">
				<a href="?page=activity&id=' . $_GET["id"] . '&accept=' . $arrMyWish["wish_id"] . '">
					<button type="button" class="btn btn-success">Yes</button>
				</a>
				<a href="?page=activity&id=' . $_GET["id"] . '&deny=' . $arrMyWish["wish_id"] . '">
					<button type="button" class="btn btn-danger">No</button>
				</a>
			</div>
		</div>';
				echo '
	</div>';
			}
		}

	}


	function displayAcceptedWishes($arrAcceptedWishes)
	{
		$controllerUser = new UserController();

		if (count($arrAcceptedWishes))
		{
			foreach ($arrAcceptedWishes as $arrMyAcceptedWish)
			{
				echo '
	<div class="panel">';
				$arrMatchWish = $this->get($arrMyAcceptedWish["match_wish_id"]);
				$arrMatchUser = $controllerUser->get($arrMatchWish["user_id"]);

				$strStartTime = ($arrMyAcceptedWish["wish_start_timestamp"] > $arrMatchWish["wish_start_timestamp"]) ? $arrMyAcceptedWish["wish_start_timestamp"] : $arrMatchWish["wish_start_timestamp"];
				$strEndTime = ($arrMyAcceptedWish["wish_end_timestamp"] < $arrMatchWish["wish_end_timestamp"]) ? $arrMyAcceptedWish["wish_end_timestamp"] : $arrMatchWish["wish_end_timestamp"];

				if ($strEndTime > date("Y-m-d H:i:s"))
				{
					if ($arrMatchWish["wish_status"] === "accepted")
					{
						echo '
		<div class="boxYesNo" style="padding: 4px;">
			<div class="left" style="font-size: 17px;">
				You have both accepted the match! You and ' . $controllerUser->displayUserInline($arrMatchUser) . ' can meet to do this activity for ' . $this->displayDateTimeInterval($strStartTime, $strEndTime) . '
			</div>
			<div class="right" style="text-align: center;">
				<a href="?page=location&id=' . $arrMyAcceptedWish["wish_id"] . '">
					<button type="button" class="btn btn-success">View location</button>
				</a>
			</div>
		</div>';
					}
					else
					{
						echo '
		<div style="font-size: 17px; padding: 4px;">
			You have accepted the match! Waiting for ' . $controllerUser->displayUserInline($arrMatchUser) . ' to accept the match for ' . $this->displayDateTimeInterval($strStartTime, $strEndTime) . '
		</div>';
					}
				}
				else
				{
					echo '
		<div style="font-size: 17px; padding: 4px;">
			We hope you and ' . $controllerUser->displayUserInline($arrMatchUser) . ' had fun (' . $this->displayDateTimeInterval($strStartTime, $strEndTime) . ')
		</div>';

					if($arrMyAcceptedWish["wish_reviewed"] === "no")
					{
						$this->displayRatingForm($arrMyAcceptedWish, $arrMatchUser);
					}
					else
					{
						echo '
        <div style="font-size: 17px; padding: 4px;">
            You have reviewed this activity!
        </div>';
					}
				}

				echo '
	</div>';
			}
		}
	}

	function displayRequestedWishes($arrRequestedWishes)
	{
		if (count($arrRequestedWishes))
		{

			foreach ($arrRequestedWishes as $arrMyWish)
			{
				echo '
	<div class="panel" style="padding: 4px;">';
				echo '
		<div style="font-size: 17px;">
			You have submitted your wish for ' . $this->displayDateTimeInterval($arrMyWish["wish_start_timestamp"], $arrMyWish["wish_end_timestamp"]) . '. You will be notified if someone is available!
		</div>';

				echo '
	</div>';
			}
		}
	}

	function displayDateTimeInterval($strDateStart, $strDateEnd)
	{
		$dateStart = new \DateTime($strDateStart);
		$dateEnd = new \DateTime($strDateEnd);

		$strStart = $this->displayDateTime($strDateStart);

		if($dateEnd->getTimestamp() - $dateStart->getTimestamp() > 3600 * 12)
			$strEnd = $dateEnd->format("l jS M G:s");
		else
			$strEnd = $dateEnd->format("G:s");
		return $strStart . " - " . $strEnd;
	}

	function displayDateTime($strDate)
	{
		$dateStart = new \DateTime($strDate);

		if($dateStart->format('Y-m-d') == (new \DateTime())->format('Y-m-d'))
			$strStart = "Today ".$dateStart->format("G:s");
		elseif($dateStart->format('Y-m-d') === (new \DateTime())->modify('+1 day')->format('Y-m-d'))
			$strStart = "Tomorrow ".$dateStart->format("G:s");
		else
			$strStart = $dateStart->format("l jS M G:s");
		return $strStart;
	}
}