<?php

namespace Controllers;
use Minishlink\WebPush\WebPush;
use Objects\User;
use ShadowCMS\BaseController;

class UserController extends BaseController
{
	function __construct()
	{
		$this->strClassName = User::class;
	}

	function create($arrData)
	{
		$this->processFile($arrData, "user_pic", "image");
		$arrData["user_salt"] = mt_rand();
		if(!isset($arrData["user_password"]))
			$arrData["user_password"] = $GLOBALS["default_password"];

		$arrData["user_password"] = hash("sha256", $arrData["user_password"].$arrData["user_salt"]);

		return parent::create($arrData);
	}



	function edit($strID, $arrData)
	{
		$this->processFile($arrData, "user_pic", "image");

		if(isset($arrData["user_password"]) && $arrData["user_password"] != "")
		{
			$arrUser = $this->get($strID);
			$arrData["user_password"] = hash("sha256", $arrData["user_password"].$arrUser["user_salt"]);
		}
		else
		{
			unset($arrData["user_password"]);
		}
		return parent::edit($strID, $arrData);
	}

	function sendPushNotification($arrUser, $arrNotification)
	{
		$subscription = json_decode($arrUser["user_subscription"], true);

		$auth = array(
			'VAPID' => array(
				'subject' => 'https://www.nobore.eu/app/demo/',
				'publicKey' => 'BEwzz-sab8DI-16tBPU9jZF1K56Z53b-kQkYcqW_vNnW06sWemJggktlwnjYdPPAI76E7S4aT8_5771eoc5kRS8',
				'privateKey' => 'yspRaYJJEKon4vzvd5q1VAbGeDXljyjZ5iKnbeBJBLo', // in the real world, this would be in a secret file
			),
		);

		$webPush = new WebPush($auth);

		$res = $webPush->sendNotification(
			$subscription['endpoint'],
			json_encode(["title" => $arrNotification["notification_name"], "message" => $arrNotification["notification_message"]]),
			$subscription['key'],
			$subscription['token'],
			true
		);
	}

	function displayUserInline($arrUser)
	{
		if(!strlen($arrUser["user_first_name"].$arrUser["user_last_name"]))
			$strDisplayName = $arrUser["user_email"];
		else
			$strDisplayName = $arrUser["user_first_name"] . ' ' . $arrUser["user_last_name"];

		return '
	<b><a href="?page=profile&id=' . $arrUser["user_id"] . '">
		<span style="font-size: 12px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
		' . $strDisplayName . '
	</a></b>';
	}
}