<?php

namespace Controllers;
use Objects\Activity;
use Objects\Event;
use Objects\Location;
use Objects\LocationReview;
use Objects\UserSession;
use ShadowCMS\BaseController;

class LocationReviewController extends BaseController
{
	function __construct()
	{
		$this->strClassName = LocationReview::class;
	}
}