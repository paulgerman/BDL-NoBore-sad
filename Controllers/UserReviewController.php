<?php

namespace Controllers;
use Objects\Activity;
use Objects\Event;
use Objects\Location;
use Objects\LocationReview;
use Objects\UserReview;
use Objects\UserSession;
use ShadowCMS\BaseController;

class UserReviewController extends BaseController
{
	function __construct()
	{
		$this->strClassName = UserReview::class;
	}
}