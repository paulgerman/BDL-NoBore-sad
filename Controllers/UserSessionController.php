<?php

namespace Controllers;
use Objects\UserSession;
use ShadowCMS\BaseController;

class UserSessionController extends BaseController
{
	function __construct()
	{
		$this->strClassName = UserSession::class;
	}
}