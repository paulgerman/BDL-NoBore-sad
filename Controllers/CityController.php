<?php

namespace Controllers;
use Objects\City;
use Objects\UserSession;
use Objects\Wish;
use ShadowCMS\BaseController;

class CityController extends BaseController
{
	function __construct()
	{
		$this->strClassName = City::class;
	}
}