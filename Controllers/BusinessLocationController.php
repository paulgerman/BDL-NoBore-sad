<?php

namespace Controllers;
use Objects\Activity;
use Objects\BusinessLocation;
use Objects\Event;
use Objects\Location;
use Objects\LocationReview;
use Objects\UserSession;
use ShadowCMS\BaseController;

class BusinessLocationController extends BaseController
{
	function __construct()
	{
		$this->strClassName = BusinessLocation::class;
	}
}