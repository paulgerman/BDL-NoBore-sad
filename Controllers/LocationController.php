<?php

namespace Controllers;
use Objects\Activity;
use Objects\Event;
use Objects\Location;
use Objects\UserSession;
use ShadowCMS\BaseController;

class LocationController extends BaseController
{
	function __construct()
	{
		$this->strClassName = Location::class;
	}

	function get_all($nStart = 0, $nLimit = 10, $arrFilter = [], $strSortCol = [], $strOrder = [])
	{
		$arrUser = $GLOBALS["session"]->arrUser;

		$arrLocationsAll = parent::get_all($nStart, $nLimit, $arrFilter, $strSortCol, $strOrder);

		if($arrUser["user_rank"] === "admin")
			return $arrLocationsAll;
		else
		{
			$controllerBusinessLocation = new BusinessLocationController();
			$arrBusinessLocations = $controllerBusinessLocation->get_all_internal(0, 9999, " WHERE `user_id` = " . $arrUser["user_id"]);

			$arrLocations = [];
			foreach ($arrBusinessLocations as $arrBusinessLocation)
			{
				if(isset($arrLocationsAll[$arrBusinessLocation["activity_location_id"]]))
					$arrLocations[$arrBusinessLocation["activity_location_id"]] = $arrLocationsAll[$arrBusinessLocation["activity_location_id"]];
			}

			return $arrLocations;
		}
	}
}