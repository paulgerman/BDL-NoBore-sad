<?php
use ShadowCMS\GeneralException;
require_once("autoload.php");
require_once("config.php");
header('Content-Type: text/html; charset=utf-8');
$arrUser = [];

$session = Session::doAll($GLOBALS["session_cookie_name"], @$_POST["email"], @$_POST["password"], @$_POST["remember"]);
if($session->isValid())
	$arrUser = $session->arrUser;
if(isset($_GET["logout"])) {
	$session->delete();
	$arrUser = [];
	header("Location: ?");
}
?>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
	<link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <title>NoBore</title>
    <?php
    if(count($arrUser))
    {?>
        <script type="text/javascript" src="app.js"></script>
    <?php
    }
    ?>
	<link href="css/style.css" rel="stylesheet">
</head>

<body style="background-color: ;">
	<div class="container">
		<?php
		if(!count($arrUser))
		{
			switch (@$_GET["page"])
			{
				case "business":
					require_once("login_business.php");
					break;
				default:
					require_once("login.php");

			}
		}
		elseif($arrUser["user_rank"] === "business")
		{
			require_once ("navbar_business.php");
			echo '
        <div style="padding-top: 50px;">';
			switch (@$_GET["page"])
			{
				case "notification":
					require_once ("notification.php");
					break;
				case "profile":
					require_once ("profile.php");
					break;
				case "location":
					require_once ("location.php");
					break;
				default:
					require_once ("dashboard_business.php");
			}
			echo '
		</div>';
        }
        else
        {
			require_once ("navbar.php");
			echo '
        <div style="padding-top: 50px;">';
			switch (@$_GET["page"])
            {
                case "activity":
	                require_once ("activity.php");
                    break;
                case "notification":
                    require_once ("notification.php");
                    break;
                case "profile":
                    require_once ("profile.php");
                    break;
                case "location":
                    require_once ("location.php");
                    break;
                case "meetings":
                    require_once ("meetings.php");
                    break;
                default:
			        require_once ("list.php");
			}
			echo '
		</div>';
		}
		?>
        <button id="push-subscription-button" style="display: none;">Push notifications !</button>
        <button id="send-push-button" style="display: none;">Send a push notification</button>
	</div>
</body>
</html>