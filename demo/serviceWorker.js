self.addEventListener('push', function (event) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    const sendNotification = body => {
        // you could refresh a notification badge here with postMessage API
        const arrNotification = JSON.parse(body);
        const title = "NoBore: " + arrNotification["title"];

        console.log(body);

        return self.registration.showNotification(title, {
            body: arrNotification["message"],
            icon: 'img/logo_normal.png',
            vibrate: [200, 100, 200, 100, 200, 100, 200],
        });
    };

    if (event.data) {
        const message = event.data.text();
        event.waitUntil(sendNotification(message));
    }
});

self.addEventListener('notificationclick', function(event) {
    console.log('[Service Worker] Notification click Received.');

    console.log(event);
    const message = event.notification.data;
    console.log(JSON.stringify(message));
    event.waitUntil(clients.openWindow('/app/demo/?page=notification'));

    event.notification.close();
});