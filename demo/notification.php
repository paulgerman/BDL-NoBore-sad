
<?php
$controllerNotification = new \Controllers\NotificationController();
$arrNotifications = $controllerNotification->get_all_internal(0,9999," WHERE `user_id` = ".(int)$arrUser["user_id"], "notification_id", "desc");
$controllerWish = new \Controllers\WishController();
foreach ($arrNotifications as $arrNotification)
{
	echo '
	<a href="'.$arrNotification["notification_link"].'">
		<div class="panel " style="padding: 5px;">
			<div class="boxNotification">
				<div class="left">
					<img src="img/notification.png" class="img-responsive" style="width: 40px">
				</div>
				<div class="right">
					'.$controllerWish->displayDateTime($arrNotification["notification_timestamp"]).'<br>
					'.$arrNotification["notification_name"].': 
					'.$arrNotification["notification_message"].'
				</div>
			</div>
		</div>
	</a>';
}

if(empty($arrNotification))
	echo '<h2 style="color:white;">You don\'t have any notifications</h2>';

$controllerUser = new \Controllers\UserController();
$controllerUser->edit($arrUser["user_id"], ["user_has_notifications" => "no"]);