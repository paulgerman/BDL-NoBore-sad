<?php
$bValidReg = true;
if(isset($_POST["password-reg"])){
	if($_POST["password-reg"] === $_POST["confirm-password"]) {
		$controllerUser = new \Controllers\UserController();
		$controllerUser->create([
			"user_email" => $_POST["email-reg"],
			"user_first_name" => $_POST["first-name"],
			"user_last_name" => $_POST["last-name"],
			"user_title" => "user",
			"user_phone" => $_POST["phone"],
			"user_password" => $_POST["password-reg"],
			"city_id" => (int)$_POST["city"],
		]);
	}
	else
		$bValidReg = false;

	header("Location: ?ok=1#");
}

$controllerCities = new \Controllers\CityController();
$arrCities = $controllerCities->get_all(0, 999999);

?>

<div class="container">
	<center>
		<div style=" margin-bottom: 30px;">
			<img src="img/logo.png" style="width: 120px; margin-bottom: 3px;">
			<center><span style="color:white; font-size: 10px; font-weight: bold;">Your day deserves more!</span></center>
		</div>
	</center>

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-6">
							<a href="#" class="active" id="login-form-link">Login</a>
						</div>
						<div class="col-xs-6">
							<a href="#register" id="register-form-link">Register</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<form id="login-form" method="post" role="form" style="display: block;">
								<?php if(isset($_POST["email"])) {?>
									<div class="alert alert-danger">
										<strong>Error!</strong> Invalid email or password!
									</div>
								<?php } ?>
								<?php if(isset($_GET["ok"])) {?>
									<div class="alert alert-success">
										<strong>Success!</strong> Your account has been created! You can now login!
									</div>
								<?php } ?>
								<div class="form-group">
									<input type="text" name="email" id="username" tabindex="1" class="form-control" placeholder="Email" value="">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
								</div>
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> Remember Me</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            <br><br>
                                           <center>
                                               <a href="?page=business">Business Login</a>
                                           </center>
										</div>
									</div>
								</div>
								<!--<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="text-center">
												<a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
											</div>
										</div>
									</div>
								</div>-->
							</form>
							<form id="register-form" method="post" action="?#register" role="form" style="display: none;">
								<?php if(!$bValidReg) {?>
									<div class="alert alert-danger">
										<strong>Error!</strong> Try again!
									</div>
								<?php } ?>
								<div class="form-group">
									<input type="email" name="email-reg" tabindex="1" class="form-control" placeholder="Email Address" value="">
								</div>
								<div class="form-group">
									<input type="text" name="first-name" tabindex="2" class="form-control" placeholder="First Name" value="">
								</div>
								<div class="form-group">
									<input type="text" name="last-name" tabindex="3" class="form-control" placeholder="Last Name" value="">
								</div>
								<div class="form-group">
									<select name="city" class="form-control" required>
										<?php
										foreach($arrCities as $arrCity)
										{
											echo '
										<option value="'.$arrCity["city_id"].'">'.$arrCity["city_name"].'</option>';
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<input type="text" name="phone" tabindex="4" class="form-control" placeholder="Phone (optional)" value="">
								</div>
								<div class="form-group">
									<input type="password" name="password-reg" tabindex="5" class="form-control" placeholder="Password">
								</div>
								<div class="form-group">
									<input type="password" name="confirm-password" tabindex="6" class="form-control" placeholder="Confirm Password">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	const fnRegister = () => {
		$("#register-form").delay(100).fadeIn(100);
		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$('#register-form-link').addClass('active');

		$(this).addClass('active');
	};
	const fnLogin = () => {
		$("#login-form").delay(100).fadeIn(100);
		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$('#login-form-link').addClass('active');

		$(this).addClass('active');
	};

	$(function() {
		$('#login-form-link').click(function(e) {
			fnLogin();
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			fnRegister();
			e.preventDefault();
		});
	});

	if(window.location.hash.substr(1) === "register")
		fnRegister();
</script>