<?php
$controllerBusinessLocation = new \Controllers\BusinessLocationController();
$arrBusinessLocations = $controllerBusinessLocation->get_all_internal(0 ,9999, " WHERE `user_id` = ".$arrUser["user_id"]);
$controllerLocation = new \Controllers\LocationController();
$controllerWish = new \Controllers\WishController();
$controllerUser = new \Controllers\UserController();
$arrAlreadyProcesses = [];
foreach($arrBusinessLocations as $arrBusinessLocation)
{
	$arrLocation = $controllerLocation->get($arrBusinessLocation["activity_location_id"]);

	$arrUpcomingWishes = $controllerWish->get_all_internal(0, 99999, " WHERE `wish_status` = 'accepted' AND `wish_start_timestamp` > NOW()");
	$arrPastWishes = $controllerWish->get_all_internal(0, 99999, " WHERE `wish_status` = 'accepted' AND `wish_start_timestamp` <= NOW()");

	$bDisplayedFuture = false;
	$bDisplayedPast = false;
	$arrDisplayUpcoming = [];
	$arrDisplayPast = [];

	foreach($arrUpcomingWishes as $arrUpcomingWish)
	{
		if(in_array($arrUpcomingWish["wish_id"], $arrAlreadyProcesses))
			continue;

		$arrMatchWish = $controllerWish->get($arrUpcomingWish["match_wish_id"]);

		if($controllerWish->getLocation($arrUpcomingWish, $arrMatchWish)["activity_location_id"] !== $arrLocation["activity_location_id"])
			continue;

		if($arrMatchWish["wish_status"] === "accepted")
		{
			$arrUserOwner = $controllerUser->get($arrUpcomingWish["user_id"]);
			$arrUserMatch = $controllerUser->get($arrMatchWish["user_id"]);

			$strStartTime = ($arrUpcomingWish["wish_start_timestamp"] > $arrMatchWish["wish_start_timestamp"]) ? $arrUpcomingWish["wish_start_timestamp"] : $arrMatchWish["wish_start_timestamp"];
			$strEndTime = ($arrUpcomingWish["wish_end_timestamp"] < $arrMatchWish["wish_end_timestamp"]) ? $arrUpcomingWish["wish_end_timestamp"] : $arrMatchWish["wish_end_timestamp"];


			$arrDisplayUpcoming[] = '
			<div class="panel" style="padding: 4px;">'.
				$controllerUser->displayUserInline($arrUserOwner). " and " . $controllerUser->displayUserInline($arrUserMatch). " will visit your place between ".$strStartTime." and ".$strEndTime.'
			</div>';
			$bDisplayedFuture = true;
		}

		$arrAlreadyProcesses[] = $arrMatchWish["wish_id"];
	}

	foreach($arrPastWishes as $arrPastWish)
	{
		if(in_array($arrPastWish["wish_id"], $arrAlreadyProcesses))
			continue;

		$arrMatchWish = $controllerWish->get($arrPastWish["match_wish_id"]);

		if($controllerWish->getLocation($arrPastWish, $arrMatchWish)["activity_location_id"] !== $arrLocation["activity_location_id"])
			continue;

		if($arrMatchWish["wish_status"] === "accepted")
		{
			$arrUserOwner = $controllerUser->get($arrPastWish["user_id"]);
			$arrUserMatch = $controllerUser->get($arrMatchWish["user_id"]);

			$strStartTime = ($arrPastWish["wish_start_timestamp"] > $arrMatchWish["wish_start_timestamp"]) ? $arrPastWish["wish_start_timestamp"] : $arrMatchWish["wish_start_timestamp"];
			$strEndTime = ($arrPastWish["wish_end_timestamp"] < $arrMatchWish["wish_end_timestamp"]) ? $arrPastWish["wish_end_timestamp"] : $arrMatchWish["wish_end_timestamp"];


			$arrDisplayPast[] = '
			<div class="panel" style="padding: 4px;">'.
				$controllerUser->displayUserInline($arrUserOwner). " and " . $controllerUser->displayUserInline($arrUserMatch). " have visited your place between ".$strStartTime." and ".$strEndTime.'
			</div>';
			$bDisplayedPast = true;
		}

		$arrAlreadyProcesses[] = $arrMatchWish["wish_id"];
	}

	if($bDisplayedPast || $bDisplayedFuture)
	{
		echo '<h2 style="color: white">Location: '.$arrLocation["activity_location_name"].'</h2>';
		if($bDisplayedFuture)
		{
			echo '<h4 style="color: white;">Upcoming</h4>';
			foreach ($arrDisplayUpcoming as $item)
				echo $item;
		}
		if($bDisplayedPast)
		{
			echo '<h4 style="color: white;">Past</h4>';
			foreach ($arrDisplayPast as $item)
				echo $item;
		}
	}
}