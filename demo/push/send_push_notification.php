<?php
require_once("autoload.php");
require_once("config.php");
use Minishlink\WebPush\WebPush;


$session = Session::doAll($GLOBALS["session_cookie_name"], @$_POST["email"], @$_POST["password"], @$_POST["remember"]);
if($session->isValid())
	$arrUser = $session->arrUser;

$subscription = json_decode($arrUser["user_subscription"], true);

$auth = array(
    'VAPID' => array(
        'subject' => 'https://www.nobore.eu/app/demo/',
        'publicKey' => 'BEwzz-sab8DI-16tBPU9jZF1K56Z53b-kQkYcqW_vNnW06sWemJggktlwnjYdPPAI76E7S4aT8_5771eoc5kRS8',
        'privateKey' => 'yspRaYJJEKon4vzvd5q1VAbGeDXljyjZ5iKnbeBJBLo', // in the real world, this would be in a secret file
    ),
);

$webPush = new WebPush($auth);

$res = $webPush->sendNotification(
    $subscription['endpoint'],
    json_encode(["title" => "TEST", "message" => "LOLOLOLOL", "link" => '../img/logo_normal.png']),
    $subscription['key'],
    $subscription['token'],
    true
);

// handle eventual errors here, and remove the subscription from your server if it is expired
