<?php
require_once("autoload.php");
require_once("config.php");

$session = Session::doAll($GLOBALS["session_cookie_name"], @$_POST["email"], @$_POST["password"], @$_POST["remember"]);
if($session->isValid())
	$arrUser = $session->arrUser;
else
	die("Not logged in!");

$controllerUser = new \Controllers\UserController();

$subscription = json_decode(file_get_contents('php://input'), true);

if (!isset($subscription['endpoint'])) {
    echo 'Error: not a subscription';
    return;
}

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'POST':
        // create a new subscription entry in your database (endpoint is unique)
	    $controllerUser->edit($arrUser["user_id"], ["user_subscription" => json_encode($subscription)]);
        break;
    case 'PUT':
        // update the key and token of subscription corresponding to the endpoint
	    $controllerUser->edit($arrUser["user_id"], ["user_subscription" => json_encode($subscription)]);
	    break;
    case 'DELETE':
        // delete the subscription corresponding to the endpoint
	    $controllerUser->edit($arrUser["user_id"], ["user_subscription" => ""]);
	    break;
    default:
        echo "Error: method not handled";
        return;
}
