
<?php
$controllerActivity = new \Controllers\ActivityController();
$controllerCities = new \Controllers\CityController();
$controllerUser = new \Controllers\UserController();

$pdo = trdb();

$arrCities = $controllerCities->get_all(0, 999999);

if(isset($_GET["city"]))
{
	$nCityId = (int) $_GET["city"];
	$controllerCities->get($nCityId);

	$arrUser = $controllerUser->edit($arrUser["user_id"], ["city_id" => $nCityId]);
}

$arrActivitiesResults = $pdo->query("SELECT `activities`.`activity_id`, `activity_name`, `activity_image` FROM `activities` LEFT JOIN activity_locations on activities.activity_id = activity_locations.activity_id WHERE `city_id` = ".(int)$arrUser["city_id"]." OR `activity_location_required` = 'no'")->fetchAll(\PDO::FETCH_ASSOC);
$arrActivities = [];
foreach ($arrActivitiesResults as $arrActivitiesResult)
	$arrActivities[$arrActivitiesResult["activity_id"]] = $arrActivitiesResult;

?>

<div class="dropdown" style="margin-bottom: 10px;">
	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		<?=$arrCities[$arrUser["city_id"]]["city_name"]?>
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		<?php
		foreach($arrCities as $arrCity)
		{
			echo '
			<li><a href="?city='.$arrCity["city_id"].'">'.$arrCity["city_name"].'</a></li>';
		}
		?>
	</ul>
</div>

<?php
if(!count($arrActivities))
	echo '<h2>No activities are available for your city.';

$i = 0;
foreach ($arrActivities as $arrActivity)
{
	echo '
	<a href="?page=activity&id='.$arrActivity["activity_id"].'">
		<div class="panel col-5-5 col-md-5 " style="padding: 4px;">
			<div class="boxList">
				<div class="left">
					<img style="width: 100%; max-height: 100px;" src="'.$arrActivity["activity_image"].'">
				</div>
				<div class="right" style="font-size: 17px; font-weight: bold;">
					'.$arrActivity["activity_name"].'
				</div>
			</div>
		</div>
	</a>';
	if($i % 2 == 0)
	    echo '<div class="col-md-1"></div>';
	$i++;
}

echo '
	';
