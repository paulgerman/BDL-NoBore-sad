<?php

$controllerUser = new \Controllers\UserController();
$arrUserShow = $arrUser;
if(isset($_GET["id"]))
	$arrUserShow = $controllerUser->get($_GET["id"]);
$pdo = trdb();

$nScoreAvg = $pdo->query("SELECT AVG(`user_review_rating`) FROM `user_reviews` WHERE `user_id_reviewed` = ".(int) $arrUserShow["user_id"])->fetchColumn(0);
if(empty($nScoreAvg))
    $nScoreAvg = 0;
?>

<div class="panel" style="padding: 4px;">
	<h4>Profile: <?=$arrUserShow["user_email"]?></h4>
    <div style="margin: auto; max-width: 140px;">
        <img style="width: 100%;" src="<?=($arrUserShow["user_pic"] == "" ? "img/default_avatar.png" : $arrUserShow["user_pic"])?>">
    </div>
    <div>
        Name: <?=$arrUserShow["user_last_name"]?><br>
        First Name: <?=$arrUserShow["user_first_name"]?><br>
        Phone: <?=$arrUserShow["user_phone"]?><br>
        Review score: <?=round($nScoreAvg,1)?>/5

    </div>

</div>
