<?php
$pdo = trdb();
$controllerActivity = new \Controllers\ActivityController();
$controllerWish = new \Controllers\WishController();
$controllerUser = new \Controllers\UserController();
$controllerNotification = new \Controllers\NotificationController();
$controllerActivityLocation = new \Controllers\LocationController();
$controllerLocationReview = new \Controllers\LocationReviewController();
$controllerUserReview = new \Controllers\UserReviewController();

$arrActivity = $controllerActivity->get($_GET["id"]);

$arrActivityLocations = $controllerActivityLocation->get_all_internal(0, 9999, " WHERE `activity_id` = " . $arrActivity["activity_id"] . " AND `city_id` = " . (int)$arrUser["city_id"]);




if (isset($_POST["newWish"]))
{
	$controllerWish->create([
		"user_id" => $arrUser["user_id"],
		"activity_id" => $arrActivity["activity_id"],
		"wish_start_timestamp" => date("Y-m-d H:i:s", strtotime($_POST["start"])),
		"wish_end_timestamp" => date("Y-m-d H:i:s", strtotime($_POST["end"])),
		"activity_location_id" => ($_POST["activity_location_id"] === "NULL" ? NULL : $_POST["activity_location_id"]),
		"city_id" => $arrUser["city_id"],

	]);
	header("Location: ?page=activity&id=" . $_GET["id"] . "&ok=1");
	exit();
}

if (isset($_GET["accept"]))
	$controllerWish->acceptWish($controllerWish->get($_GET["accept"]));


if(isset($_POST["review"]))
    $controllerWish->doRating();

$arrMyRequestedWishes = $controllerWish->getMyRequestedWishes($_GET["id"]);
$arrMyPendingWishes = $controllerWish->getMyPendingWishes($_GET["id"]);
$arrMyAcceptedWishes = $controllerWish->getMyAcceptedWishes($_GET["id"]);

$controllerWish->runMatchingAlgorithm($arrMyRequestedWishes);

echo '
<br>';

echo '
<div class="panel" style="padding: 4px;">
	<div class="boxList">
		<div class="left">
			<img style="width: 100px;" src="' . $arrActivity["activity_image"] . '">
		</div>
		<div class="right">
			' . $arrActivity["activity_name"] . '<br><br>
			' . $arrActivity["activity_description"] . '
		</div>
	</div>

</div>';

$controllerWish->displayAcceptedWishes($arrMyAcceptedWishes);

$controllerWish->displayPendingWishes($arrMyPendingWishes);

$controllerWish->displayRequestedWishes($arrMyRequestedWishes);

$dateStartDefault = (new DateTime())->setTime(18, 0, 0);
$dateEndDefault = (new DateTime())->setTime(20, 0, 0);

echo '
<div class="panel" style="padding: 10px;">
	<h4>When are you available for this activity?</h4>
	<form class="form-horizontal" method="post">
	  <input type="hidden" name="newWish" value="1">
	  <div class="form-group">
	    <label class="control-label col-sm-2" for="start">Starting from:</label>
	    <div class="col-sm-10">
	      <input type="datetime-local" class="form-control" name="start" id="start" placeholder="Time" value="' . $dateStartDefault->format('Y-m-d\TH:i:s') . '">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label class="control-label col-sm-2" for="end">End at:</label>
	    <div class="col-sm-10">
	      <input type="datetime-local" class="form-control" name="end" id="end" placeholder="Time" value="' . $dateEndDefault->format('Y-m-d\TH:i:s') . '">
	    </div>
	  </div>
	  
        <div class="form-group">
            <label class="control-label col-sm-2" for="location">Location:</label>
            <div class="col-sm-10">
                <div class="list-group">';
if (!count($arrActivityLocations))
	echo "<h4>We don't have any predefined locations for this event.<br> You will need to arrange with your match.</h4>";
else
{
	echo '
				<a href="#" class="list-group-item active" onclick="return selectLocation(this);">
					<div class="radio">
	                    <label><input type="radio" name="activity_location_id" value="NULL" checked>No preferred location</label>
					</div>
				</a>';
	foreach ($arrActivityLocations as $arrActivityLocation)
	{
		$nScoreAvg = $pdo->query("SELECT AVG(`activity_location_review_rating`) FROM `activity_location_reviews` WHERE `activity_location_id` = ".(int) $arrActivityLocation["activity_location_id"])->fetchColumn(0);
		if(empty($nScoreAvg))
			$nScoreAvg = 0;

		echo '
				<a href="#" class="list-group-item" onclick="return selectLocation(this);">
					<div class="radio">
	                    <label>
	                        <input type="radio" name="activity_location_id" value="' . $arrActivityLocation["activity_location_id"] . '">' . $arrActivityLocation["activity_location_name"] . ' (score: '.round($nScoreAvg, 1).'/5)
                        </label>
                        <div style="float: right">
                            <button onclick="window.open(\'?page=location&location_id=' . $arrActivityLocation["activity_location_id"] . '\', \'_blank\');" type="button" class="btn btn-success" style="margin-top: -10px;">View location</button>
                        </div>
					</div>
				</a>';
	}
}
echo '
			</div>
	    </div>
	  </div>
	  

	  <div class="form-group"> 
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-default">Submit</button>
	    </div>
	  </div>
	</form>
</div>';
?>

<script>
	function selectLocation(el)
	{
		$(el).parent().children().removeClass("active");
		$(el).addClass("active");
		setTimeout(() => $(el).find("input[type='radio']").prop("checked", true), 5);
		return false;
	}
</script>

<?php