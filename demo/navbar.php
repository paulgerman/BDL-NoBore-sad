<?php
$strPage = isset($_GET['page'])?$_GET['page']:"";
?>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header" style="padding-left: 10px">
			<button type="button" class="pull-left navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="?"><img src="img/logo.png" style="width: 80px; margin-top: -7px; margin-left: -15px;"></a>
            <?php
            if($arrUser["user_has_notifications"] == "yes")
                echo '
                <a href="?page=notification" style="text-decoration: none;" onclick="window.location.href=\'?page=notification\'">
                    <div data-toggle="collapse" class="navbar-toggle" style="padding: 6px 10px 2px 10px;">
                            <span class="glyphicon glyphicon-flag notification"></span>
                            <span style="vertical-align: top;font-size: 13px;color:white; margin-left: 2px; margin-top: 5px;">NEW</span>
                    </div>
                </a>'
            ?>

		</div>
		<div class="collapse navbar-collapse" id="myNavbar" style="overflow: hidden">
			<ul class="nav navbar-nav">
				<li <?=$strPage == ''?'class="active"':''?>><a href="?">Home</a></li>
				<li <?=$strPage == 'meetings'?'class="active"':''?>><a href="?page=meetings">My Meetings</a></li>
				<li <?=$strPage == 'notification'?'class="active"':''?>>
                    <a href="?page=notification">
                        Notifications
	                    <?=$arrUser["user_has_notifications"]=="yes"?'<span class="glyphicon glyphicon-flag notification" style="font-size: 15px;"></span>':''?>
                    </a>
                </li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
                <?php
                if($arrUser["user_rank"] === "admin")
                {?>

                <li><a href="editUsers.php" target="_blank"><span class="glyphicon glyphicon-th"></span> Users</a></li>
                <li><a href="editActivities.php" target="_blank"><span class="glyphicon glyphicon-film"></span> Activities</a></li>

	                <?php
                }
                ?>
				<li <?=$strPage == 'profile'?'class="active"':''?>><a href="?page=profile"><span class="glyphicon glyphicon-user"></span> <?=$arrUser["user_email"];?></a></li>
				<li><a href="?logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</div>
</nav>