<?php

?>

<div class="container">
	<center>
		<div style=" margin-bottom: 30px;">
			<img src="img/logo.png" style="width: 120px; margin-bottom: 3px;">
			<center><span style="color:white; font-size: 10px; font-weight: bold;">Your day deserves more!</span></center>
		</div>
        <center style="color: white; font-size: 21px;">For Business</center><br>
	</center>

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="active" id="login-form-link">Login</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<form id="login-form" method="post" role="form" style="display: block;">
								<?php if(isset($_POST["email"])) {?>
									<div class="alert alert-danger">
										<strong>Error!</strong> Invalid email or password!
									</div>
								<?php } ?>
								<div class="form-group">
									<input type="text" name="email" id="username" tabindex="1" class="form-control" placeholder="Email" value="">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
								</div>
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> Remember Me</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            <br><br>
                                            <center>
                                                <a href="?">User Login</a>
                                            </center>
                                        </div>
									</div>
								</div>
								<!--<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="text-center">
												<a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
											</div>
										</div>
									</div>
								</div>-->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>