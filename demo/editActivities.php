<?php
require_once("autoload.php");
require_once("config.php");
$arrUser = [];
$bLoggedIn = false;
$session = Session::doAll($GLOBALS["session_cookie_name"], @$_POST["email"], @$_POST["password"], @$_POST["remember"]);
if($session->isValid())
{
	$arrUser = $session->arrUser;
	$bLoggedIn = true;
}

if(!$bLoggedIn || array_key_exists("user_rank", $arrUser) && $arrUser['user_rank'] !== "admin" && $arrUser["user_rank"] !== "business")
{
	header('Location: index.php?page=login');
	exit();
}
?>
<head>
	<meta charset="UTF-8">
	<?php
	require_once ("frontEnd/ShadowJS/Renderer/includeAll.php");
	echo loadObjConfigs([
		Objects\Activity::class,
		Objects\Location::class,
	]);
	?>
</head>
<body>
<div class="container" id="container">
</div>
<div class="container">
</div>

<script>
	<?php
	echo "var arrUser = ".json_encode($arrUser).";\n";
	?>

	var renderer = new Activity(document.getElementById("container"), [], arrRenderConfigs);
	renderer.init();
</script>

</body>
