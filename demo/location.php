<?php
$bFound = false;
$controllerLocation = new \Controllers\LocationController();
$controllerWish = new \Controllers\WishController();


if(isset($_GET["id"]))
{
	$arrWish = $controllerWish->get($_GET["id"]);
	$arrMatchWish = $controllerWish->get($arrWish["match_wish_id"]);

	try
	{
		$arrLocation = $controllerWish->getLocation($arrWish, $arrMatchWish);
		$bFound = true;
	}
	catch (\ShadowCMS\GeneralException $exc)
	{
		if ($exc->getCode() !== \ShadowCMS\GeneralException::PRODUCT_NOT_FOUND)
			throw $exc;
	}
}
elseif(isset($_GET["location_id"]))
{
	$arrLocation = $controllerLocation->get($_GET["location_id"]);
	if($arrLocation["activity_location_images"] !== '')
		$arrLocation["activity_location_images"] = explode(",", $arrLocation["activity_location_images"]);
	else
		$arrLocation["activity_location_images"] = [];
	$bFound = true;
}

if (!$bFound)
	echo '<h1> NO LOCATIONS DEFINED FOR THIS EVENT</h1>';
else
{
	?>
    <div class="panel" style="padding: 10px;">

        <h3>Location: <?= $arrLocation["activity_location_name"] ?></h3>

        <h4>Address: <?= $arrLocation["activity_location_address"] ?></h4>
        <h4>Phone: <?= $arrLocation["activity_location_phone"] ?></h4>

        <div class="wysiwyg">
			<?= $arrLocation["activity_location_description"] ?>
        </div>
    </div>
	<?php
	if (count($arrLocation["activity_location_images"]))
	{
		?>
        <div class="panel" style="padding: 4px;">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
					<?php
					foreach ($arrLocation["activity_location_images"] as $i => $strImage)
						echo '
            <li data-target="#carousel-example-generic" data-slide-to="' . $i . '" ' . ($i === 0 ? 'class="active"' : '') . '></li>
                '
					?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
					<?php
					foreach ($arrLocation["activity_location_images"] as $i => $strImage)
						echo '
			<div class="item ' . ($i === 0 ? 'active' : '') . '">
				<img src="' . $strImage . '" style="display: block; margin: auto; height: 250px;">
			</div>';
					?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
		<?php
	}

	?>
    <div class="panel" style="padding: 4px;">
        <div style="margin-bottom: 5px;" id="map"></div>

        <center>
            <a href="geo:<?= $arrLocation["activity_location_lat"] ?>,<?= $arrLocation["activity_location_lng"] ?>"
               target="_blank">
                <button type="button" class="btn btn-success">Open in maps</button>
            </a>
        </center>
    </div>

    <script>
		function initMap()
		{
			var uluru = {
				lat: <?=$arrLocation["activity_location_lat"]?>,
				lng: <?=$arrLocation["activity_location_lng"]?>};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 15,
				center: uluru
			});
			var marker = new google.maps.Marker({
				position: uluru,
				map: map
			});
		}
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrly6pqQXsaMBGTX8JHx5koV6Tum9_AAw&callback=initMap">
    </script>

    <script>
		$(".carousel").swipe({

			swipe: function (event, direction, distance, duration, fingerCount, fingerData) {

				if (direction == 'left') $(this).carousel('next');
				if (direction == 'right') $(this).carousel('prev');

			},
			allowPageScroll: "vertical"

		});
    </script>
	<?php
}
?>
