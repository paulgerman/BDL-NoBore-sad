<?php

$controllerUser = new \Controllers\UserController();
$controllerWish = new \Controllers\WishController();

if (isset($_GET["accept"]))
	$controllerWish->acceptWish($controllerWish->get($_GET["accept"]));

if(isset($_POST["review"]))
	$controllerWish->doRating();

$controllerActivity = new \Controllers\ActivityController();
$arrActivities = $controllerActivity->get_all_internal(0, 999999);

$controllerWish->runMatchingAlgorithm($controllerWish->getMyRequestedWishes());

$bDisplayed = false;
foreach ($arrActivities as $arrActivity)
{
	$arrMyRequestedWishes = $controllerWish->getMyRequestedWishes($arrActivity["activity_id"]);
	$arrMyPendingWishes = $controllerWish->getMyPendingWishes($arrActivity["activity_id"]);
	$arrMyAcceptedWishes = $controllerWish->getMyAcceptedWishes($arrActivity["activity_id"]);

	if(!empty($arrMyAcceptedWishes) || !empty($arrMyPendingWishes) || !empty($arrMyRequestedWishes))
    {
        echo '<h2 style="color: white;">Activity '.$arrActivity["activity_name"].'</h2>';
	    $controllerWish->displayAcceptedWishes($arrMyAcceptedWishes);

	    $controllerWish->displayPendingWishes($arrMyPendingWishes);

	    $controllerWish->displayRequestedWishes($arrMyRequestedWishes);

	    $bDisplayed = true;
    }
}

if(!$bDisplayed)
	echo '<h3 style="color: white;">You don\'t have any meetings. You can go <b><a href="?">Home</a></b>, select an activity and submit a wish.</h3>';
