<?php
CONST VERSION = 2;
$arrConfigDB = [
	"host" => "localhost",
	"port" => "3307",
	"DB" => "nobore",
	"username" => "root",
	"password" => ""
];
\ShadowCMS\CustomPDO\CustomPDO::setConfig($arrConfigDB);
\ShadowCMS\CustomPDO\CustomPDO::singleton()->enableLogging();
$bDebug = true;

$GLOBALS["relative_path"] = str_replace("\\",'/',substr(getcwd(),strlen($_SERVER['DOCUMENT_ROOT']) + 1));
if($GLOBALS["relative_path"] !== '')
	$GLOBALS["relative_path"] = $GLOBALS["relative_path"].'/';

$GLOBALS["session_cookie_name"] = "session_nobore";
$GLOBALS["default_password"] = "test";