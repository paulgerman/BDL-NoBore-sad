<?php
error_reporting(E_ALL &~ E_STRICT &~ E_DEPRECATED);
assert_options(ASSERT_BAIL, 1);
ini_set("display_errors", 1);
ini_set("error_log", "logs/php-error.log");

$arrAllowedPages = array(
	"privateEndpoint" => array(
		"autoload.php",
		"config.php",
		"Endpoints/PrivateEndpoint.php",
	),
	"publicEndpoint" => array(
		"autoload.php",
		"config.php",
		"Endpoints/PublicEndpoint.php",
	),
	"test.php" => array("test.php"),
	"b.html" => array("b.html"),
);

$arrAllowedFolders = array(
	"frontEnd",
	"uploadedImages",
	"uploadedFiles",
	"demo",
);

$strRequest = $_GET["r"];
unset($_GET["r"]);

if(array_key_exists($strRequest, $arrAllowedPages))
{
	foreach($arrAllowedPages[$strRequest] as $strInclude)
	{

		if(file_exists($strInclude))
		{
			require_once($strInclude);
		}
		else
		{
			throw new Exception("File ".$strInclude." not found!");
		}
	}
}
else
{
	foreach($arrAllowedFolders as $strFolder)
	{
		if(strpos($strRequest, $strFolder) === 0)
		{
			if(is_file($strRequest))
			{
				require_once($strRequest);
			}
			elseif(is_dir($strRequest))
			{
				$strRequest .= DIRECTORY_SEPARATOR;
				if(file_exists($strRequest."index.html"))
					require_once($strRequest."index.html");
				else if(file_exists($strRequest."index.php"))
					require_once($strRequest."index.php");
				else
				{
					header("HTTP/1.0 404 Not Found");
					die("no index page in folder ".$strFolder." | ".$strRequest);
				}
			}
			else
			{
				header("HTTP/1.0 404 Not Found");
				die($strRequest." not found");
			}
			exit();
		}
	}
	header("HTTP/1.0 404 Not Found");
	die("Page requested does not exist!");
}
