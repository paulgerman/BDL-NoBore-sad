<?php
spl_autoload_register(
	function($strClassName)
	{
		$strFileName = __DIR__.DIRECTORY_SEPARATOR.str_replace("\\", DIRECTORY_SEPARATOR, $strClassName).".php";
		if(file_exists($strFileName))
			require_once($strFileName);

		if(strpos($strFileName, "\\Endpoints"))
			echo $strFileName;
	}
);
require_once ("classes/ShadowCMS/autoload.php");
require_once ("classes/Session.php");

require __DIR__ . '/vendor/autoload.php';
