<?php

use ShadowCMS\GeneralException;

class Session
{
	public $arrSession;
	public $arrUser;
	private $controllerUserSession;
	private $controllerUser;
	private $strCookieName;

	function __construct($strCookieName)
	{
		$this->controllerUserSession = new \Controllers\UserSessionController();
		$this->controllerUser = new \Controllers\UserController();
		$this->arrSession = array();
		$this->strCookieName = $strCookieName;
	}

	function create($strUserEmail, $strPassword, $bRemember)
	{
		if($bRemember)
			$nValidSeconds = 3600*24*7;
		else
			$nValidSeconds = 3600;

		$arrUser = $this->controllerUser->get(["user_email" => $strUserEmail]);
		if(strlen($arrUser["user_password"]) === 32)
			$strPassword = hash("md5", $strPassword.$arrUser["user_salt"]);
		else
			$strPassword = hash("sha256", $strPassword.$arrUser["user_salt"]);

		if($strPassword === $arrUser["user_password"])
		{
			$this->arrSession = $this->controllerUserSession->create(array(
				"user_id" => $arrUser["user_id"],
				"user_session_end_date" => date('Y-m-d H:i:s', time() + $nValidSeconds),
				"user_session_start_date" => date('Y-m-d H:i:s', time()),
				"user_session_hash" => $arrUser["user_id"].'|'.hash("sha256", uniqid(rand(), true)),
				"user_session_ip" => $_SERVER["REMOTE_ADDR"]
			));
			$this->arrUser = $arrUser;
		}
		else
		{
			throw new GeneralException("Invalid password", GeneralException::INVALID_PASSWORD);
		}
	}

	function storeCookie()
	{
		setcookie($GLOBALS["session_cookie_name"], $this->arrSession["user_session_hash"], time()+60*60*24*30, '/');
	}

	private function deleteCookie()
	{
		unset($_COOKIE[$GLOBALS["session_cookie_name"]]);
		setcookie($GLOBALS["session_cookie_name"], '', time()-3600, '/');
	}

	function delete()
	{
		if($this->isValid())
		{
			$this->arrSession["user_session_terminated"] = 1;
			$this->arrSession = $this->controllerUserSession->edit($this->arrSession["user_session_id"], $this->arrSession);
		}
		$this->arrSession = [];
		$this->arrUser = [];
		if(isset($GLOBALS["session"])) unset($GLOBALS["session"]);
		$this->deleteCookie();
	}

	function load($strSessionHash)
	{
		try{
			$this->arrSession = $this->controllerUserSession->get(["user_session_hash" => $strSessionHash]);
			$this->arrUser = $this->controllerUser->get($this->arrSession["user_id"]);
		}
		catch(GeneralException $exc)
		{
			if($exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND)
				throw $exc;
		}
	}

	function isValid()
	{
		if(
			array_key_exists("user_session_end_date", $this->arrSession)
			&& array_key_exists("user_session_hash", $this->arrSession)
			&& array_key_exists("user_session_start_date", $this->arrSession)
			&& array_key_exists("user_session_terminated", $this->arrSession))
		{
			if($this->arrSession["user_session_terminated"] == 1)
				return false;

			$nEndTime = strtotime($this->arrSession["user_session_end_date"]);
			if($nEndTime > time() && strlen($this->arrSession["user_session_hash"]))
			{
				$nExtendTime = 1800;
				if($nEndTime - $nExtendTime < time())
				{
					$this->arrSession["user_session_end_date"] = date("Y-m-d H:i:s", time() + $nExtendTime);
					$this->arrSession = $this->controllerUserSession->edit($this->arrSession["user_session_id"], $this->arrSession);
				}
				return true;
			}
		}
		return false;
	}


	static function doAll($strCookieName, $strUserEmail, $strPassword, $bRemember)
	{
		$session = new Session($strCookieName);
		if(array_key_exists($strCookieName, $_COOKIE))
		{
			$session->load($_COOKIE[$strCookieName]);
		}

		if(!is_null($strUserEmail) && !is_null($strPassword))
		{
			if(is_null($bRemember))
				$bRemember = false;
			try
			{
				$session->create($strUserEmail, $strPassword, $bRemember);
			}
			catch(GeneralException $exc)
			{
				if(
					$exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND
					&& $exc->getCode() !== GeneralException::INVALID_PASSWORD
				)
					throw $exc;
			}
			if($session->isValid())
			{
				$session->storeCookie();
				header("Refresh:0");
			}
		}
		return $session;
	}
}