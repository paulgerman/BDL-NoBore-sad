<?php
namespace Objects;


use ShadowCMS\BaseObject;

class Notification extends BaseObject
{

	public static $strObjectName = "notification";
	public static $strIndexProp = "notification_id";
	public static $strTableName = "notifications";

	public static $arrProps = array(
		"notification_id",
		"notification_name",
		"notification_message",
		"notification_timestamp",
		"notification_link",
		"user_id"
	);


	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
	);

	public static $arrPropsReadOnly = array(
		"notification_timestamp"
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}