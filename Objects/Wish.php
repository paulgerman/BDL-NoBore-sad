<?php
namespace Objects;

use ShadowCMS\BaseObject;

class Wish extends BaseObject
{

	public static $strObjectName = "wish";
	public static $strIndexProp = "wish_id";
	public static $strTableName = "wishes";

	public static $arrProps = array(
		"wish_id",
		"user_id",
		"activity_id",
		"wish_start_timestamp",
		"wish_end_timestamp",
		"wish_status",
		"match_wish_id",
		"activity_location_id",
		"city_id",
		"wish_reviewed"
	);


	public static $arrPropsEnum = array(
		"wish_reviewed" => ["yes", "no"]
	);

	public static $arrPropsOptional = array(
		"wish_status" => "requested",
		"match_wish_id" => 0,
		"wish_reviewed" => "no"
	);

	public static $arrPropsReadOnly = array(
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}