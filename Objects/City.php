<?php
namespace Objects;

use ShadowCMS\BaseObject;

class City extends BaseObject
{

	public static $strObjectName = "city";
	public static $strIndexProp = "city_id";
	public static $strTableName = "cities";

	public static $arrProps = array(
		"city_id",
		"city_name",
		"country_id"
	);


	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
	);

	public static $arrPropsReadOnly = array(
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}