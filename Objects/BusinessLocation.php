<?php
namespace Objects;


use ShadowCMS\BaseObject;

class BusinessLocation extends BaseObject
{

	public static $strObjectName = "business_location";
	public static $strIndexProp = "business_location_id";
	public static $strTableName = "business_locations";

	public static $arrProps = [
		"business_location_id",
		"user_id",
		"activity_location_id",
	];

	public static $arrPropsEnum = [
	];

	public static $arrPropsOptional = [
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
	];

	public $arrValues = [];

}