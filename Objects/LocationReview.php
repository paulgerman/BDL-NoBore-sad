<?php
namespace Objects;


use ShadowCMS\BaseObject;

class LocationReview extends BaseObject
{

	public static $strObjectName = "locationReview";
	public static $strIndexProp = "activity_location_review_id";
	public static $strTableName = "activity_location_reviews";

	public static $arrProps = [
		"activity_location_review_id",
		"user_id",
		"activity_location_review_rating",
		"activity_location_id",
	];

	public static $arrPropsEnum = [
	];

	public static $arrPropsOptional = [
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
	];

	public $arrValues = [];

}