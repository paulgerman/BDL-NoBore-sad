<?php
namespace Objects;


use ShadowCMS\BaseObject;

class UserSession extends BaseObject
{

	public static $strObjectName = "user_session";
	public static $strIndexProp = "user_session_id";
	public static $strTableName = "user_sessions";

	public static $arrProps = array(
		"user_session_id",
		"user_id",
		"user_session_start_date",
		"user_session_end_date",
		"user_session_hash",
		"user_session_ip",
		"user_session_terminated"
	);


	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
		"user_session_terminated" => 0
	);

	public static $arrPropsReadOnly = array(
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}