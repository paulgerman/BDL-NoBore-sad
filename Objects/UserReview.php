<?php
namespace Objects;


use ShadowCMS\BaseObject;

class UserReview extends BaseObject
{

	public static $strObjectName = "userReview";
	public static $strIndexProp = "user_review_id";
	public static $strTableName = "user_reviews";

	public static $arrProps = [
		"user_review_id",
		"user_id",
		"user_review_rating",
		"user_id_reviewed",
	];

	public static $arrPropsEnum = [
	];

	public static $arrPropsOptional = [
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
	];

	public $arrValues = [];

}