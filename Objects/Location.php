<?php
namespace Objects;


use ShadowCMS\BaseObject;

class Location extends BaseObject
{

	public static $strObjectName = "location";
	public static $strIndexProp = "activity_location_id";
	public static $strTableName = "activity_locations";

	public static $arrProps = [
		"activity_id",
		"activity_location_id",
		"activity_location_name",
		"activity_location_description",
		"activity_location_address",
		"activity_location_lat",
		"activity_location_lng",
		"activity_location_phone",
		"activity_location_images",
		"city_id"
	];

	public static $arrPropsEnum = [
	];

	public static $arrPropsOptional = [
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
		"activity_location_id" => [
			"displayName" => "ID",
		],
		"activity_location_name" => [
			"displayName" => "Name",
			"internalAttributes" => [
				"required" => true
			]
		],
		"activity_location_address" => [
			"displayName" => "Address",
			"internalAttributes" => [
				"required" => true
			]
		],
		"activity_location_lat" => [
			"displayName" => "Latitude",
			"internalAttributes" => [
				"required" => true,
				"type" => "number",
				"step" => "any",
			],
		],
		"activity_location_lng" => [
			"displayName" => "Longitude",
			"internalAttributes" => [
				"required" => true,
				"type" => "number",
				"step" => "any",
			],
		],
		"activity_location_phone" => [
			"displayName" => "Phone number",
			"internalAttributes" => [
				"required" => true,
				"type" => "number"
			],
		],
		"activity_location_description" => [
			"displayName" => "Description",
			"internalAttributes" => [
				"required" => true
			],
			"internalType" => "BootstrapWYSIWYG",
			"maxDisplaySize" => 70,
		],
		"activity_location_images" => [
			"displayName" => "Images",
			"tooltip" => "(MULTIPLE - drag & drop to reorder)",
			"internalType" => "MultiImage",
			"type" => "file",
			"maxWidthOrHeight" => 700,
			"internalAttributes" => [
				"required" => true
			]
		],
		"city_id" => [
			"displayName" => "City ID",
			"internalAttributes" => [
				"type" => "number"
			]
		]
	];

	public $arrValues = [];

}