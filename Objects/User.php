<?php
namespace Objects;


use ShadowCMS\BaseObject;

class User extends BaseObject
{

	public static $strObjectName = "user";
	public static $strIndexProp = "user_id";
	public static $strTableName = "users";

	public static $arrProps = [
		"user_id",
		"user_email",
		"user_first_name",
		"user_last_name",
		"user_title",
		"user_phone",
		"user_pic",
		"user_rank",
		"user_password",
		"user_salt",
		"user_has_notifications",
		"user_subscription",
		"city_id"
	];

	public static $arrPropsEnum = [
		"user_rank" => ["user", "admin", "business"],
		"user_has_notifications" => ["yes", "no"]
	];

	public static $arrPropsOptional = [
		"user_rank" => "user",
		"user_salt" => "",
		"user_password" => "",
		"user_phone" => "",
		"user_pic" => "",
		"user_has_notifications" => "no",
		"user_subscription" => "",
		"city_id" => NULL
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
		"user_id" => [
			"displayName" => "ID",
		],
		"user_email" => [
			"displayName" => "Email",
			"internalAttributes" => [
				"required" => true,
				"type" => "email"
			],
			"tooltip" => "(default password: test)",

		],
		"user_first_name" => [
			"displayName" => "Last name",
			"internalAttributes" => [
				"required" => true
			]
		],
		"user_last_name" => [
			"displayName" => "First name",
			"internalAttributes" => [
				"required" => true
			]
		],
		"user_phone" => [
			"displayName" => "Phone",
			"tooltip" => "(optional)",
		],
		"user_title" => [
			"displayName" => "Title",
			"internalAttributes" => [
				"required" => true
			]
		],
		"user_pic" => [
			"displayName" => "Photo",
			"internalType" => "Image",
			"type" => "file",
			"maxWidthOrHeight" => 500,
		],
		"user_rank" => [
			"displayName" => "Rank",
			"type" => "select",
		],
		"user_has_notifications" => [
			"displayName" => "Has notifications?",
			"type" => "select",
		],

	];

	public $arrValues = [];

}