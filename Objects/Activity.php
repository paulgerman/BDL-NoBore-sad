<?php
namespace Objects;


use ShadowCMS\BaseObject;

class Activity extends BaseObject
{

	public static $strObjectName = "activity";
	public static $strIndexProp = "activity_id";
	public static $strTableName = "activities";

	public static $arrProps = [
		"activity_id",
		"activity_name",
		"activity_image",
		"activity_description",
		"activity_location_required"
	];

	public static $arrPropsEnum = [
		"activity_location_required" => ["yes", "no"]
	];

	public static $arrPropsOptional = [
		"activity_location_required" => "no"
	];

	public static $arrPropsReadOnly = [
	];

	public static $arrRenderProps = [
		"activity_id" => [
			"displayName" => "ID",
		],
		"activity_name" => [
			"displayName" => "Name",
			"internalAttributes" => [
				"required" => true
			]
		],
		"activity_image" => [
			"displayName" => "Image",
			"internalAttributes" => [
				"required" => true
			],
			"maxWidthOrHeight" => 500,
			"internalType" => "Image",
			"type" => "file",
		],
		"activity_description" => [
			"displayName" => "Description",
			"internalAttributes" => [
				"required" => true
			]
		],
		"activity_location_required" => [
			"displayName" => "Location required?",
			"internalAttributes" => [
				"required" => true
			],
			"type" => "select"
		]
	];

	public $arrValues = [];

}